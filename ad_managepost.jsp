<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Cheyyaru Classified</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="resources/images/favicon.ico">

        <link src="resources/plugins/morris/morris.css" rel="stylesheet">

        <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="resources/css/icons.css" rel="stylesheet" type="text/css">
        <link href="resources/css/style.css" rel="stylesheet" type="text/css">
 		<link src="resources/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link src="resources/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link src="resources/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<style>
.f1{

    th { font-size: 13px; }

td { font-size: 13px; }
}
 .col-md-12.md-content {
    padding:0!important;

}.md-content > div {
    padding: 0!important;}
	.wi {
    width: 50%;
    float: left;
}.width50{width:33%;float:left;}
.md-show.md-effect-11 .md-content{
display:inline-block;padding-bottom: 30px!important;
}
.col-md-6 {
    float: left;
}.control-label.col-sm-6 {
    max-width: 100% !important;
}.wi2 {
    width: 95%;
    float: left;    margin-left: 5%;
}
.pl1 {
    float: right;margin-top: 1.25rem;
margin-right: 1.00rem;
}.flt{
float:right;
}
</style>
    </head>


    
    <body>

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">





                   <div class="col-md-2 pull-left">
<a class="navbar-brand" href="#"><img class="img-responsive" src="resources/assets/images/logo.png" alt="Logo"></a>
</div>
                      <div class="col-md-2 pull-right">  <ul class="list-inline float-right mb-0">
                            
                            <!-- language-->


                          

                    
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="resources/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
<span>${user_name}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Welcome</h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>
                                   
                                   
                                    <a class="dropdown-item" href="login.html"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                               
                            </li>
                        

                        </ul>
</div>


                   
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>
                </div>
            </div>

                    <div class="navbar-custom">
                    	 <div class="container-fluid">
               
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                         
                            <li class="has-submenu">
                                <a href="admin_page.html"><i class="mdi mdi-bullseye"></i>User Management</a>
                               
                            </li>

                            <li class="has-submenu active">
                                <a href="ad_managepost.html"><i class="mdi mdi-gauge"></i>Post Management</a>
                               
                            </li>

 </li>
                              <li class="has-submenu">
                                <a href="admin_request.html"><i class="mdi mdi-layers"></i>Request Management</a>
                               
                            </li>



                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                
            </div> 

                </div> <!-- end container -->
        </header>
        <!-- End Navigation Bar-->



        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Overview</a></li>
                                    <li class="breadcrumb-item active">Add PostDetails</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Add Details</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->


<div class="page-body">
<div class="row">
<div class="col-sm-12">
<div class="card">
<a class="flt" href="ad_create.html"><button type="button" class="btn btn-primary btn-md waves-effect waves-light pull-right pl1">Create Post Details</button></a>
</br>
</br>
<div class="card-block">
<div class="dt-responsive table-responsive" style = "padding: 10px;">
<table id="datatable" class="table table-striped table-bordered nowrap" style="width: 100%">
<thead>
<tr>
<th>S.no</th>
		<th>Title</th>
		<th>Name</th>

		<th>Email</th>
		<th>mobile</th>
        <th>Area</th>
        <th>Category</th>
		<th>Active From</th>
		<th>Active To</th>

		<th>Status</th>
		<th>Action</th>
</tr>
</thead>
<tbody>
</tbody>

</table>
<!-- Modal -->


</div>




</div>
</div>







</div>
</div>
</div>

                    </div>

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        Â© TechnoKryon
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="resources/js/jquery.min.js"></script>
        <script src="resources/js/popper.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/modernizr.min.js"></script>
        <script src="resources/js/waves.js"></script>
        <script src="resources/js/jquery.slimscroll.js"></script>
        <script src="resources/js/jquery.nicescroll.js"></script>
        <script src="resources/js/jquery.scrollTo.min.js"></script>

        <script src="resources/plugins/skycons/skycons.min.js"></script>
        <script src="resources/plugins/raphael/raphael-min.js"></script>
        <script src="resources/plugins/morris/morris.min.js"></script>

        <script src="resources/pages/dashborad.js"></script>

        <!-- App js -->
        <script src="resources/js/app.js"></script>
        <!-- Required datatable js -->
        <script src="resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="resources/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="resources/plugins/datatables/jszip.min.js"></script>
        <script src="resources/plugins/datatables/pdfmake.min.js"></script>
        <script src="resources/plugins/datatables/vfs_fonts.js"></script>
        <script src="resources/plugins/datatables/buttons.html5.min.js"></script>
        <script src="resources/plugins/datatables/buttons.print.min.js"></script>
        <script src="resources/plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="resources/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="resources/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <!-- Datatable init js -->
        <script src="resources/pages/datatables.init.js"></script>


<script type="text/javascript">
var d = ${postlist};
console.log(d);

  var table = $('#datatable').DataTable({
	"processing" : true,
	"data" : ${postlist},
	 "bSort": false,
   "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

    		 var index = iDisplayIndex +1;

var status = aData.status;
console.log(status);

   /*  if(status == "Active"){

					 $('td:eq(9)',nRow).html('<div class="label-main"> <label class="btn btn-success" style = "width: 70px; text-align: center;">Active</label> </div>');

   			 }

   	if(status == "Deactive"){

			 $('td:eq(9)',nRow).html('<div class="label-main"> <label  class="btn btn-danger" style = "width: 80px; text-align: center;">Deactive</label> </div>');

               }
 */

			$('td:eq(10)', nRow).html('<form action = "post_view.html" method="post"><input type="hidden" name="userid" value='+aData.id+'><button type="submit" class="btn btn-info waves-effect md-trigger">Update</button></a>');

         },

	"columns" :  [{
		"defaultContent" : '<div style="color: #000000;"></div>',
	}, {
		"data" : "title",
		"defaultContent" : "<i>-</i>",
	},{
		"data" : "name"
	},{
      		"data" : "emailid",
      			"defaultContent" : "<i>-</i>",
    },{
		"data" : "phoneno",
			"defaultContent" : "<i>-</i>",
	},{
       		"data" : "areaMaster.area",
       		"defaultContent" : "<i>-</i>",
   	},{
         	"data" : "categoryMaster.category",
         	"defaultContent" : "<i>-</i>",
   	},{
               	"data" : "fromdate"
   	},{
               	"data" : "todate"
    },{
               	"data" : "statusMaster.status"
    },{
     		"defaultContent" : '',
     		"className" : 'edit-control',
        },

	]
	});

	 table.on( 'order.dt search.dt', function () {
		 table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	 $('#datatable tbody').on('click', 'tr', function() {
			var d = table.row(this).data();
			    });



</script>
    </body>

</html>
