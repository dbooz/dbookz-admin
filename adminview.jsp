<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>

<!-- Mirrored from mannatthemes.com/annex/horizontal/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Mar 2018 06:59:54 GMT -->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Cheyyaru Classified</title>
<meta content="Admin Dashboard" name="description" />
<meta content="Mannatthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link rel="shortcut icon" href="resources/images/favicon.ico">

<link src="resources/plugins/morris/morris.css" rel="stylesheet">

<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<link src="resources/plugins/datatables/dataTables.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link src="resources/plugins/datatables/buttons.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link src="resources/plugins/datatables/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />


<style>
.form-btns div {
	display: inline-block;
}

.col-md-12.md-content {
	padding: 0 !important;
}

.page-body {
	overflow-x: hidden;
}

.btn-primary, .sweet-alert button.confirm, .wizard>.actions a {
	margin-top: 20px !important
}

.md-content>div {
	padding: 0 !important;
}

.wi {
	width: 50%;
	float: left;
}

.pl1 {
	float: right;
	margin-top: 1.25rem;
	margin-right: 1.25rem;
}

.flt {
	float: right
}

.width50 {
	width: 33%;
	float: left;
}

.md-show.md-effect-11 .md-content {
	display: inline-block;
	padding-bottom: 20px !important
}

.col-md-6 {
	float: left;
}

.control-label.col-sm-6 {
	max-width: 100% !important;
}

.wi2 {
	width: 95%;
	float: left;
	margin-left: 5%;
}

code {
	padding: 0;
	text-align: center;
}
</style>
</head>


    <body>

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">





                   <div class="col-md-2 pull-left">
<a class="navbar-brand" href="#"><img class="img-responsive" src="resources/assets/images/logo.png" alt="Logo"></a></div>
                      <div class="col-md-2 pull-right">  <ul class="list-inline float-right mb-0">
                            
                            <!-- language-->


                          

                    
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="resources/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
<span>${user_name}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Welcome</h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>
                                   
                                   
                                    <a class="dropdown-item" href="login.html"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                               
                            </li>
                        

                        </ul>
</div>


                   
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>
                </div>
            </div>

                    <div class="navbar-custom">
                    	 <div class="container-fluid">
               
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                      

                            <li class="has-submenu active">
                                <a href="admin_page.html"><i class="mdi mdi-bullseye"></i>User Management</a>
                               
                            </li>

                            <li class="has-submenu">
                                <a href="ad_managepost.html"><i class="mdi mdi-gauge"></i>Post Management</a>
                               
                            </li>

 </li>
                              <li class="has-submenu">
                                <a href="admin_request.html"><i class="mdi mdi-layers"></i>Request Management</a>
                               
                            </li>


                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                
            </div> 

                </div> <!-- end container -->
        </header>
        <!-- End Navigation Bar-->


	<div class="wrapper" style="margin-top: 30px;">
		<div class="container-fluid">

			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">
						<div class="btn-group pull-right">
							<ol class="breadcrumb hide-phone p-0 m-0">
								<li class="breadcrumb-item"><a href="#">Overview</a></li>
								<li class="breadcrumb-item active">Add SubAdmin</li>
							</ol>
						</div>
						<h4 class="page-title">Update SubAdmin</h4>
					</div>
				</div>
			</div>
			<!-- end page title end breadcrumb -->



			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<form class="" action="update_admin.html" method="post">
							<div class="card ">





								<div class="card-block">

									<div class="createmgt">


										<center>
											<h5 style="display: contents; color: green;">${message}</h5>
										</center>



										<div class="form-group">

											<div class="col-sm-12">
												<input type="hidden" class="form-control" id="userid"
													placeholder="" name="userid" value="${userid}">
											</div>
										</div>


										<div class="form-group">

											<label class="control-label col-sm-2" for="email">Name:</label>
											<div class="col-sm-12">
												<input type="text" class="form-control" id="name"
													placeholder="" name="name" value="${name}"
													required="required">
											</div>
										</div>


										<div class="form-group">
											<label class="control-label col-sm-6" for="pwd">
												Email:</label>
											<div class="col-sm-12">
												<input type="text" class="form-control" id="pwd"
													placeholder="" name="emailid" value="${emailid}" readonly>
											</div>

										</div>
										<div class="form-group">
											<label class="control-label col-sm-6" for="pwd">
												Mobile No:</label>
											<div class="col-sm-12">
												<input type="number" class="form-control" id="pwd"
													placeholder="" min="0" name="phoneno" value="${phoneno}"
													required="required">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-6" for="pwd">
												Address:</label>
											<div class="col-sm-12">
												<input type="text" class="form-control" id="pwd"
													placeholder="" name="address" value="${address}">
											</div>

										</div>



										<div class="form-group">
											<label class="control-label col-sm-6 " for="pwd">
												Area:</label>
											<div class="col-sm-12">
												<select name="area" class="form-control" id="areadrop"
													required="required">


												</select>
											</div>

										</div>

									</div>
								</div>



								<div class="clearfix"></div>
								<div class="col-md-6 form-btns"
									style="float: right !important; text-align: right;">
									<div>
										<button type="submit" class="btn btn-primary waves-effect "
											id="Update" style="margin-bottom: 20px; margin-top: 10px">Update</button>

									</div>
						</form>
						<div>
							<form id="Deactive" action="a_deactive.html" method="post">

								<input type="hidden" class="form-control" id="userid"
									placeholder="" name="userid" value="${userid}">


								<button type="submit" class="btn btn-primary waves-effect "
									style="margin-bottom: 20px; margin-top: 10px">Deactive</button>

							</form>
						</div>
						<div>
							<form id="Reactive" action="a_reactive.html" method="post">

								<input type="hidden" class="form-control" id="userid"
									placeholder="" name="userid" value="${userid}">
								<button type="submit" class="btn btn-primary waves-effect "
									style="margin-bottom: 20px; margin-top: 10px">Reactive</button>

							</form>

						</div>

					</div>
				</div>
			</div>
		</div>



	</div>
	</div>
	</div>

	</div>

	</div>
	<!-- end container -->
	</div>
	</div>
	</div>
	<!-- end wrapper -->


	<!-- Footer -->
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">© TechnoKryon</div>
			</div>
		</div>
	</footer>
	<!-- End Footer -->



	<input id="area" value="${area}" type="hidden">
	<!-- jQuery  -->
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/popper.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/modernizr.min.js"></script>
	<script src="resources/js/waves.js"></script>
	<script src="resources/js/jquery.slimscroll.js"></script>
	<script src="resources/js/jquery.nicescroll.js"></script>
	<script src="resources/js/jquery.scrollTo.min.js"></script>

	<script src="resources/plugins/skycons/skycons.min.js"></script>
	<script src="resources/plugins/raphael/raphael-min.js"></script>
	<script src="resources/plugins/morris/morris.min.js"></script>

	<script src="resources/pages/dashborad.js"></script>

	<!-- App js -->
	<script src="resources/js/app.js"></script>
	<!-- Required datatable js -->
	<script src="resources/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
	<!-- Buttons examples -->
	<script src="resources/plugins/datatables/dataTables.buttons.min.js"></script>
	<script src="resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
	<script src="resources/plugins/datatables/jszip.min.js"></script>
	<script src="resources/plugins/datatables/pdfmake.min.js"></script>
	<script src="resources/plugins/datatables/vfs_fonts.js"></script>
	<script src="resources/plugins/datatables/buttons.html5.min.js"></script>
	<script src="resources/plugins/datatables/buttons.print.min.js"></script>
	<script src="resources/plugins/datatables/buttons.colVis.min.js"></script>
	<!-- Responsive examples -->
	<script src="resources/plugins/datatables/dataTables.responsive.min.js"></script>
	<script src="resources/plugins/datatables/responsive.bootstrap4.min.js"></script>

	<!-- Datatable init js -->
	<script src="resources/pages/datatables.init.js"></script>

	<script>

 var jsonData = ${arealist};
 console.log(jsonData);

     var jsonList ={jsonData}

    $(document).ready(function(){
      var listItems= "";
     // console.log(arealist);
       listItems+= "<option value='' disabled='' selected=''>--Select--</option>";
      for (var i = 0; i < jsonList.jsonData.length; i++){

        listItems+= "<option value='" + jsonList.jsonData[i].id + "'>" + jsonList.jsonData[i].area + "</option>";
      }

      $("#areadrop").html(listItems);

    });

</script>

	<script type="text/javascript">

		var val = $('#areadrop').val();

			$("#areadrop option").prop('selected', false).filter(function() {
				return $(this).text() == val;
			}).prop('selected', true);

			console.log(val);
		</script>


	<script type="text/javascript">

               var status=${status};
               console.log(status);
               console.log(status);

                 if(status=="Deactive"){
                	$('#Deactive').hide();
                	$('#Update').hide();

            	  console.log(0);
               }
                if(status=="Active"){
              	  console.log(0);
              	$('#Reactive').hide();

               }

                </script>

	<script type="text/javascript">
 $(document).ready(function(){

		var val = $('#area').val();
         console.log(val);
		$("#areadrop").val(val);
			});
		</script>


</body>

</html>

