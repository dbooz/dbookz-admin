<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Cheyyaru Classified</title>
<meta content="Admin Dashboard" name="description" />
<meta content="Mannatthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="resources/images/favicon.ico">
<link src="resources/plugins/morris/morris.css" rel="stylesheet">
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<link src="resources/plugins/datatables/dataTables.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link src="resources/plugins/datatables/buttons.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link src="resources/plugins/datatables/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link href="resources/css/editor.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="resources/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap-daterangepicker/css/daterangepicker.css" />
<link rel="stylesheet" type="text/css"
	href="resources/css/datedropper.min.css" />
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="resources/css/xzoom.css">

<style>
.fl1 {
	margin: 5px auto;
	float: left;
}

.createmgt {
	width: 90%;
	margin: 0 auto;
}

.control-label.col-sm-6.fl1 {
	margin-top: 16px;
}

.f1 {
	float: right;
}

.required:after {
	content: " *";
}

#map {
	height: 100%;
}

#map #infowindow-content {
	display: inline;
}

#pac-input {
	top: 10% !important;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 20%;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 400px;
	height: 40px;
}
</style>
</head>

<body>

	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div>

	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container-fluid">





				<div class="col-md-2 pull-left">
<a class="navbar-brand" href="#"><img class="img-responsive" src="resources/assets/images/logo.png" alt="Logo"></a>				</div>
				<div class="col-md-2 pull-right">
					<ul class="list-inline float-right mb-0">

						<!-- language-->





						<!-- User-->
						<li class="list-inline-item dropdown notification-list"><a
							class="nav-link dropdown-toggle arrow-none waves-effect nav-user"
							data-toggle="dropdown" href="#" role="button"
							aria-haspopup="false" aria-expanded="false"> <img
								src="resources/images/users/avatar-1.jpg" alt="user"
								class="rounded-circle"> <span>${user_name}</span>
						</a>
							<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
								<!-- item-->
								<div class="dropdown-item noti-title">
									<h5>Welcome</h5>
								</div>
								<a class="dropdown-item" href="#"><i
									class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a> <a
									class="dropdown-item" href="login.html"><i
									class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a></li>


					</ul>
				</div>



				<!-- end menu-extras -->

				<div class="clearfix"></div>
			</div>
		</div>

		<div class="navbar-custom">
			<div class="container-fluid">

				<div id="navigation">
					<!-- Navigation Menu-->
					<ul class="navigation-menu">
				
						<li class="has-submenu"><a href="admin_page.html"><i
								class="mdi mdi-bullseye"></i>User Management</a></li>

						<li class="has-submenu"><a href="ad_managepost.html"><i
								class="mdi mdi-gauge"></i>Post Management</a></li>


 </li>
                              <li class="has-submenu active">
                                <a href="admin_request.html"><i class="mdi mdi-layers"></i>Request Management</a>
                               
                            </li>

					</ul>
					<!-- End navigation menu -->
				</div>
				<!-- end #navigation -->

			</div>

		</div>
		<!-- end container -->
	</header>
	<!-- End Navigation Bar-->


	<div class="wrapper">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">
						<div class="btn-group pull-right">
							<ol class="breadcrumb hide-phone p-0 m-0">
								<li class="breadcrumb-item"><a href="#">Overview</a></li>
								<li class="breadcrumb-item active">Add PostDetails</li>
							</ol>
						</div>
						<h4 class="page-title">Add Details</h4>
					</div>
				</div>
			</div>
			<!-- end page title end breadcrumb -->
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<form class="" action="req_update.html" method="post"
							 enctype="multipart/form-data">
							<div class="card ">
								<div class="card-block">
								
								<center><h6 style="display: contents; color: green;">${message}</h6></center>
								
									<div class="createmgt" style="margin-top: 30px;">
										<div class="form-group">
											<label class="control-label col-sm-4 fl1 required" for="pwd">
												Title:</label>
											<div class="row">
												<div class="col-sm-6 fl1">
													<input type="text" name="title" class="form-control "
														id="pwd" value="${title}" placeholder="In English"
														required="required">
												</div>
												<div class="col-sm-6 fl1">
													<input type="text" name="title_t" class="form-control "
														id="pwd" value="${title_t}" placeholder="In Tamil">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">
													Name:</label>
												<div class="row">
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="pwd"
															value="${name}" placeholder="In English" name="name"
															required="required">
													</div>
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="pwd"
															value="${name_t}" placeholder="In Tamil" name="name_t">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">
													Email:</label>
												<div class="row">
													<div class="col-sm-6">
														<input type="text" class="form-control " id="pwd"
															value="${emailid}" name="emailid">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">
													Mobile No1:</label>
												<div class="row">
													<div class="col-sm-6">
														<input type="number" class="form-control " id="pwd"
															value="${phoneno}" name="phoneno" min="0">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">
													Mobile No2:</label>
												<div class="row">
													<div class="col-sm-6">
														<input type="number" class="form-control " id="phoneno2"
															value="${phoneno2}" name="phoneno2" min="0">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">
													Mobile No3:</label>
												<div class="row">
													<div class="col-sm-6">
														<input type="number" class="form-control " id="phoneno3"
															value="${phoneno3}" name="phoneno3" min="0">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required">
													Landmark:</label>
												<div class="row">
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control "
															placeholder="In English" value="${landmark}"
															name="landmark" required="required">
													</div>
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control "
															placeholder="In Tamil" value="${landmark_t}"
															name="landmark_t">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required">
													Address:</label>
												<div class="row">
													<div class="col-sm-6 fl1">
														<textarea type="text" id="address" class="form-control "
															placeholder="In English" name="address"
															required="required">${address}</textarea>
													</div>
													<div class="col-sm-6 fl1">
														<textarea type="text" id="address_t" class="form-control "
															placeholder="In Tamil" name="address_t">${address_t}</textarea>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required">
													Location:</label>
												<div class="row">
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="location"
															placeholder="In English" value="${location}"
															name="location" required="required">
													</div>
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="location_t"
															placeholder="In Tamil" value="${location_t}"
															name="location_t">
													</div>
												</div>
											</div>
											<div class="form-group" style="height: 500px;">
												<input id="pac-input" class="controls" type="text"
													placeholder="Search Box">
												<div id="map"></div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required">
													Latitude:</label>
												<div class="row">
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="id_lat"
															placeholder="Latitude" value="${lattitude}"
															name="lattitude" onkeydown="return false">
													</div>
													<div class="col-sm-6 fl1">
														<input type="text" class="form-control " id="id_lang"
															placeholder="Longitude" value="${longitude}"
															name="longitude" onkeydown="return false">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required" for="pwd">Area
													:</label>
												<div class="row">
													<div class="col-sm-6">
														<select name="area" class="form-control" id="area"
															required="required">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required" for="pwd">Category
													:</label>
												<div class="row">
													<div class="col-sm-6">
														<select name="category" class="form-control" id="category"
															required="required">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group" data-date-format="dd-mm-yyyy">
												<label class="control-label col-sm-4 fl1 required">From</label>
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control" type="text"
															placeholder="DD-MM-YYYY" id="div_nv" name="fromdate"
															value="${fromdate}" onkeydown="return false"
															required="required"> <span class="add-on"></span>
													</div>
												</div>
											</div>
											<div class="form-group" data-date-format="dd-mm-yyyy">
												<label class="control-label col-sm-4 fl1 required" for="pwd">To
													:</label>
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control" type="text"
															placeholder="DD-MM-YYYY" id="todate" name="todate"
															value="${todate}" onkeydown="return false"
															required="required"> <span class="add-on"></span>
													</div>
												</div>
											</div>






											<div class="form-group">


												<label class="control-label col-sm-4 fl1 required" for="pwd">Image
													:</label>
												<div class="row">
													<div class="col-sm-12">


														<img class="xzoom" src="${image}" id="image"
															alt="Not Available" /> <a id='remove' href="#"
															class="btn btn-primary waves-effect ">Remove</a>

														<%-- <input type = "hidden" id = "images" name = "image" value = "${image }"> --%>
													</div>
												</div>
											</div>





											<div class="form-group">
												<label class="control-label col-sm-4 fl1" for="pwd">Change
													Image :</label>
												<div class="row">
													<div class="col-sm-6">
														<input name="image" type="file" id="image1"
															accept="image/x-png,image/jpeg">

													</div>
												</div>
											</div>




											<div class="form-group">
												<label class="control-label col-sm-4 fl1 required" for="pwd">Description(English)
													:</label>
												<div class="row">
													<div class="col-sm-12">
														<textarea id="txtEditorEnglish" class="form-control"
															name="description">${description}</textarea>

													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4 fl1 " for="pwd">Description(Tamil)
													:</label>
												<div class="row">
													<div class="col-sm-12">
														<textarea id="txtEditorTamil" class="form-control"
															name="description_t" placeholder="EX.description"
															required="required" min="5">${description_t}
                                                  </textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
	<input type="hidden" name="post_id" value="${post_id}">
 
	
									 <div class="clearfix"></div>
										<div class="col-md-12 "
											style="float: right !important; text-align: center;"> 
											<div>
												<button type="submit" class="btn btn-primary waves-effect "
													id="Update" style="margin-bottom: 20px; margin-top: 10px">Approve</button>
</div>
										
											
											
											</form>

<%-- 
<div class = "col-sm-6" style = "float : right;">
						<div>
							<form id="Deactive" action="post_deactive.html" method="post">

								<input type="hidden" class="form-control" id="userid"
									placeholder="" name="userid" value="${userid}">


								<button type="submit" class="btn btn-primary waves-effect "
									style="margin-bottom: 20px; margin-top: 10px">Deactive</button>

							</form>
						</div>
						<div>
							<form id="Reactive" action="a_reactive.html" method="post">

								<input type="hidden" class="form-control" id="userid"
									placeholder="" name="userid" value="${userid}">
								<button type="submit" class="btn btn-primary waves-effect "
									style="margin-bottom: 20px; margin-top: 10px">Reactive</button>

							</form>

					
					
					
					</div> --%>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<!-- end container -->
	</div>
	<!-- end wrapper -->
	<!-- Footer -->
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">© TechnoKryon</div>
			</div>
		</div>
	</footer>
	<!-- End Footer -->


	<input type="hidden" value="${area}" id="areadrop" name="area">
	<input type="hidden" value="${category}" id="categorydrop" name="area">




	<!-- jQuery  -->

	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/xzoom.min.js"></script>

	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/popper.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/modernizr.min.js"></script>
	<script src="resources/js/waves.js"></script>
	<script src="resources/js/jquery.slimscroll.js"></script>
	<script src="resources/js/jquery.nicescroll.js"></script>
	<script src="resources/js/jquery.scrollTo.min.js"></script>
	<script src="resources/plugins/skycons/skycons.min.js"></script>
	<script src="resources/plugins/raphael/raphael-min.js"></script>
	<script src="resources/plugins/morris/morris.min.js"></script>
	<script src="resources/pages/dashborad.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- App js -->
	<script src="resources/js/app.js"></script>
	<!-- Required datatable js -->
	<script src="resources/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
	<!-- Buttons examples -->
	<script src="resources/plugins/datatables/dataTables.buttons.min.js"></script>
	<script src="resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
	<script src="resources/plugins/datatables/jszip.min.js"></script>
	<script src="resources/plugins/datatables/pdfmake.min.js"></script>
	<script src="resources/plugins/datatables/vfs_fonts.js"></script>
	<script src="resources/plugins/datatables/buttons.html5.min.js"></script>
	<script src="resources/plugins/datatables/buttons.print.min.js"></script>
	<script src="resources/plugins/datatables/buttons.colVis.min.js"></script>
	<!-- Responsive examples -->
	<script src="resources/plugins/datatables/dataTables.responsive.min.js"></script>
	<script src="resources/plugins/datatables/responsive.bootstrap4.min.js"></script>
	<!-- Datatable init js -->
	<script src="resources/pages/datatables.init.js"></script>

	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBq6-G6B4SHP9Uc-6NbCmz59hr7PXQRnk&libraries=places&callback=initAutocomplete"
		async defer></script>

	<script src="resources/ckeditor/ckeditor.js"></script>

	<script>
         $(document).ready(function() {
         CKEDITOR.replace('txtEditorEnglish');

         });


      </script>

	<script>
               $(document).ready(function() {
               CKEDITOR.replace('txtEditorTamil');

               });


            </script>
	<script>
         // This example adds a search box to a map, using the Google Place Autocomplete
         // feature. People can enter geographical searches. The search box will return a
         // pick list containing a mix of places and predicted search terms.

         // This example requires the Places library. Include the libraries=places
         // parameter when you first load the API. For example:
         // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
         var marker = null;
         var status = true;



         function initAutocomplete() {
           var map = new google.maps.Map(document.getElementById('map'), {
             center: {lat: 12.6650429, lng: 79.537460},
             zoom: 8,
             mapTypeId: 'roadmap'
           });

         var infowindow = new google.maps.InfoWindow(
         {
         size: new google.maps.Size(150,50)
         });


         google.maps.event.addListener(map, 'click', function() {
           infowindow.close();
           });


         google.maps.event.addListener(map, 'click', function(event) {
         //call function to create marker
            if (marker) {
               marker.setMap(null);
               marker = null;
            }
         var latlng = ""+event.latLng;
         latlng = latlng.substr(1).slice(0, -1);
         var str_array = latlng.split(',');

         console.log(str_array);

         document.getElementById('id_lat').value = str_array[0].trim();
         document.getElementById('id_lang').value = str_array[1].trim();
         marker = createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng);
         console.log(event.latLang);
         });

         // A function to create the marker and set up the event window function
         function createMarker(latlng, name, html) {
         var contentString = html;
         var marker = new google.maps.Marker({
           position: latlng,
           map: map,
           zIndex: Math.round(latlng.lat()*-100000)<<5
           });

         google.maps.event.addListener(marker, 'click', function() {
           infowindow.setContent(contentString);
           infowindow.open(map,marker);
           });
                
         google.maps.event.trigger(marker, 'click');
         return marker;
         }
         
    
     


           // Create the search box and link it to the UI element.
           var input = document.getElementById('pac-input');
           var searchBox = new google.maps.places.SearchBox(input);
           map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

           // Bias the SearchBox results towards current map's viewport.
           map.addListener('bounds_changed', function() {
             searchBox.setBounds(map.getBounds());
           });

           var markers = [];
           // Listen for the event fired when the user selects a prediction and retrieve
           // more details for that place.
           searchBox.addListener('places_changed', function() {
             var places = searchBox.getPlaces();

             if (places.length == 0) {


               return;
             }

         var geocoder = new google.maps.Geocoder();
           var address = document.getElementById('pac-input').value;

           geocoder.geocode({ 'address': address }, function (results, status) {

               if (status == google.maps.GeocoderStatus.OK) {
                   var latitude = results[0].geometry.location.lat();
                   var longitude = results[0].geometry.location.lng();
              console.log(latitude);
         console.log(longitude);
         document.getElementById('id_lat').value = latitude;
         document.getElementById('id_lang').value = longitude;

         if (marker) {
               marker.setMap(null);
               marker = null;
            }
         // double dlatitude = Double.parseDouble(latitude);
           // double dlongitude = Double.parseDouble(longitude);
         // LatLng latlng = new LatLng(dlatitude, dlongitude);
         marker = createMarker(results[0].geometry.location, "name", "<b>Location</b><br>"+results[0].geometry.location);

               }
           });
                    });
           
         
         
         	 
         }

         

         
      </script>
	<script>
         var jsonData = ${arealist};
         console.log(jsonData);


            $(document).ready(function(){
              var listItems= "";
             // console.log(arealist);
               listItems+= "<option value='' disabled='' selected=''>--Select--</option>";
              for (var i = 0; i < jsonData.length; i++){

                listItems+= "<option value='" + jsonData[i].id + "'>" + jsonData[i].area + "</option>";
              }

              $("#area").html(listItems);

            });

      </script>
	<script>
         var category = ${categorylist};
         console.log(category);

         var jsonList ={category}

          $(document).ready(function(){
            var listItems= "";
             listItems+= "<option value='' disabled='' selected=''>--Select--</option>";
            for (var i = 0; i < jsonList.category.length; i++){

              listItems+= "<option value='" + jsonList.category[i].id + "'>" + jsonList.category[i].category + "</option>";
            }

            $("#category").html(listItems);

          });



      </script>
	<script>
         var date = $('#div_nv').datepicker({


          dateFormat: 'dd-mm-yy',
          changeMonth: true,
         	changeYear: true,
           minDate: 0

          });

      </script>
	<script>
         var date = $('#todate').datepicker({


          dateFormat: 'dd-mm-yy',
          changeMonth: true,
         	changeYear: true,
           minDate: 0

          });
      </script>

	<script>
      function loadDescription(){

console.log(2);
           var dfg = $('#txtEditorEnglish').val();
           console.log($('#txtEditorEnglish'));
      }
            </script>



	<script>

 $(document).ready(function(){

	   var value = $('#categorydrop').val();
          console.log(value);
            $("#category").val(value);

            });
</script>

	<script>
 $(document).ready(function(){

		var val = $('#areadrop').val();
         console.log(val);
		$("#area").val(val);

});
</script>

	<script type="text/javascript">
            
 $(function rmv() {
	    $('#remove').click(function() {
	            $('#image').remove();
	            
	        });

	}); 
	    
     </script>

	<script type="text/javascript">       
          
 //   var value = $('#images').val();
    var value = ${image};
    
           if(value == null ){
              	  console.log(0);
              	$('#remove').hide();

               }
            
              </script>
	<script type="text/javascript">
    var val = $('#phoneno2').val(); 
    console.log(val);
    </script>



</body>

</html>